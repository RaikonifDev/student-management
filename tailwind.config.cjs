/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        blueOne: "#0046ff",
        blueTwo: "#326afe",
        blueThree: "#6282f3",
        blueFour: "#84A5FC",
        redOne: "#f03636",
        redTwo: "#e96155",
        greenOne: "#2faf53",
        greenTwo: "#2dcc70",
        grayOne: "#a6a6a6",
        orangeOne: "#F68A1C",
        whiteOne: "#fafafa",
        whiteTwo: "#f9f9f9",
      },
    },
    fontFamily: {
      rale: ["'Raleway'", "'sans-serif'"],
    },
  },
  plugins: [],
};
