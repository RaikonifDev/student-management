import RoutesApp from "./router";

function App() {
  return (
    <div className="w-full h-full mx-auto">
      <RoutesApp />
    </div>
  );
}

export default App;
