export interface IUsersManagement {
  id: number;
  name: string;
  email: string;
  active: boolean;
  user_management: boolean;
}
