export interface INavbarItem {
    name: string,
    url: string,
    blackIconUrl: string,
    whiteIconurl: string,
}