import { IProgram } from "./program";
import { IUsersManagement } from "./userManagement";

export interface IAction {
  type: string;
  payload: any;
}

export interface IProgramState {
  programs: IProgram[];
}

export interface IUsersManagementState {
  usersManagement: IUsersManagement[];
}
