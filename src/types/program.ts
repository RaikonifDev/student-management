export interface IProgram{
    id: number,
	program_name: string,
	students: number,
	time_spent: number,
	status: number

}