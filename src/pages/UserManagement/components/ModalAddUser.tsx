import React from "react";
import Submit from "../../../components/Buttons/Submit";
import DarkBackground from "../../../components/DarkBackground";
import Form from "../../../components/Form";
import Field from "../../../components/Form/Field";

interface IProps {
  action: Function;
  setTo: any;
}

const ModalAddUser = ({ action, setTo }: IProps) => {
  return (
    <DarkBackground>
      <Form
        title="Add User"
        description="Complete the Form"
        action={action}
        setTo={setTo}
      >
        <Field
          labelText="Names"
          name="names"
          type="text"
          placeholder="Juan P"
        />
        <Field
          labelText="Surnames"
          name="surnames"
          type="text"
          placeholder="Perez"
        />
        <Field
          labelText="Email"
          name="email"
          type="email"
          placeholder="user.jala@jalauniversity.edu"
        />
        <Submit />
      </Form>
    </DarkBackground>
  );
};

export default ModalAddUser;
