import React, { useState } from "react";
import Add from "../../../components/Buttons/Add";
import Profile from "../../../components/Buttons/Profile";

interface IProps {
  action: Function;
  setTo: any;
}

function Header({ action, setTo }: IProps) {
  const [isVisible, setIsvisible] = useState(false);
  return (
    <div className="flex justify-between items-center mb-7">
      <div>
        <h1 className="font-black md:text-3xl mt-5 tracking-wide mb-3">
          Users Management
        </h1>
        <p className="text-lg tracking-wide font-light">12 users found</p>
      </div>
      <div className="flex gap-2 items-center">
        <Add action={action} setTo={setTo}>
          New User
        </Add>
        <Add action={action} setTo={setTo}>
          Import Users
        </Add>
        <div className="">
          <img
            src="./src/assets/img/ImageGuy.png"
            alt="Image Guy"
            className="object-cover h-10 w-10 rounded-xl shadow-sm mx-2"
            onClick={() => setIsvisible(!isVisible)}
          />
          {isVisible && <Profile />}
        </div>
      </div>
    </div>
  );
}

export default Header;
