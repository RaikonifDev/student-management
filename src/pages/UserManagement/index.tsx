import React, { useState } from "react";
import { useQuery } from "@tanstack/react-query";

import DataGridTable from "../../components/DataGridTable/Table";

import { getUsers } from "../../service/usersManagement.service";
import { IUsersManagement } from "../../types/userManagement";
import { useUsersManagementContext } from "./context";

import Add from "../../components/Buttons/Add";
import ModalAddUser from "./components/ModalAddUser";
import Header from "./components/Header";

function UserManagement() {
  const [state, actions] = useUsersManagementContext();
  const [modalAddUser, setModalAddUser] = useState(false);
  const { data, refetch } = useQuery({
    queryKey: ["getUsers"],
    queryFn: getUsers,
    staleTime: 60000,
    onSuccess: (usersManagement: IUsersManagement[]) => {
      actions.onAddUsers(usersManagement);
    },
  });

  const headers = ["id", "Name", "Email", "Active", "User Management"];

  return (
    <>
      <div className="p-4">
        <Header action={setModalAddUser} setTo={true}></Header>
        <div className="flex flex-col">
          <div className="overflow-x-auto sm:-mx-6 lg:-mx-23">
            <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
              <div className="overflow-hidden">
                <DataGridTable headers={headers} data={state.usersManagement} />
              </div>
            </div>
          </div>
        </div>
        <div className="flex justify-between items-center">
          <p>Showing 1-7 from 132 data</p>
          <div className="flex gap-1">
            <button className="font-black text-xl px-4  rounded-full border-2 border-gray-500">
              &lt;
            </button>
            <div className="flex border-2 border-gray-500 rounded-full gap-3 overflow-hidden">
              <button className="px-4 text-sm rounded-full text-white bg-blueOne">
                1
              </button>
              <button className="p-2 px-4 ">2</button>
              <button className="p-2 px-4 ">3</button>
            </div>
            <button className="font-black text-xl px-4 rounded-full border-2 border-gray-500">
              &gt;
            </button>
          </div>
        </div>
      </div>
      {/* {modalAddProgram ? (
        <ModalAddProgram action={setModalAddProgram} setTo={false} />
      ) : (
        ""
      )} */}
      {/* <Upload>Import Users</Upload>
        {modalAddUser ? (
          <ModalAddUser action={setModalAddUser} setTo={false} />
        ) : (
          ""
        )} */}
    </>
  );
}

export default UserManagement;
