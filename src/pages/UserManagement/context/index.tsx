import {
  createContext,
  useContext,
  useReducer,
  useState,
  ReactNode,
} from "react";

import { State, Reducer } from "./store";
import { IUsersManagementState } from "../../../types/context";
import UsersActionFactory from "./actions";

const HomeContext = createContext<null | IUsersManagementState>(null);
const HomeDistpach = createContext<any>(null);

interface IProps {
  children: ReactNode;
}

export function UsersManagementContextProvider(props: IProps) {
  const { children } = props;
  const [value, dispatch] = useReducer(Reducer, props, State);
  const [actions] = useState(UsersActionFactory(dispatch));
  return (
    <HomeContext.Provider value={value}>
      <HomeDistpach.Provider value={actions}>{children}</HomeDistpach.Provider>
    </HomeContext.Provider>
  );
}

export function useUsersManagementContext() {
  const state = useContext(HomeContext);
  const actions = useContext(HomeDistpach);
  return [state, actions];
}
