import UserManagement from "..";
import typeUserManagement from "./dispatchs";
import { IUsersManagement } from "../../../types/userManagement";

function onAddUsers(dispatch: any, payload: IUsersManagement[]) {
  dispatch({ type: typeUserManagement.addUser, payload: payload });
}

const UsersActionFactory = (dispatch: any) => {
  return {
    onAddUsers: (payload: IUsersManagement[]) => onAddUsers(dispatch, payload),
  };
};

export default UsersActionFactory;
