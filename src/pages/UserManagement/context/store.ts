import typesUsersManagement from "./dispatchs";
import { IUsersManagement } from "../../../types/userManagement";
import { IUsersManagementState, IAction } from "../../../types/context";

function State(): IUsersManagementState {
  return {
    usersManagement: new Array<IUsersManagement>(),
  };
}

function Reducer(state: IUsersManagementState, action: IAction) {
  const { type, payload } = action;
  switch (type) {
    case typesUsersManagement.addUser: {
      return {
        ...state,
        userManagement: payload,
      };
    }

    default:
  }
}

export { Reducer, State };
