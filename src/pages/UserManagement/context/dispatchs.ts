const namespace = "UserManagement";

const UserManagementDispachsTypes = {
  addUser: `${namespace}.addUser`,
};

export default UserManagementDispachsTypes;
