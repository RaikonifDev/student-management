import { useContext } from "react";
import { AuthContext } from "../../context/AuthContext";
import { Navigate, useNavigate } from "react-router-dom";

function DashBoard() {
  const { sesion, handleLogout } = useContext(AuthContext);
  const navigate = useNavigate();

  const closeSesion = () => {
    handleLogout();
    // service keycloak desroy sesion

    navigate("/login");
  };

  if (!sesion.estado) {
    return <Navigate to="/login" />;
  }

  return (
    <>
      <div className="bg-gray-600">1</div>
      <div className="bg-gray-400">2</div>
      <div className="bg-gray-400">3</div>
      {sesion.estado !== false ? sesion.credentials.username : ""}
      <div className="bg-gray-200 col-span-3">4</div>
      <div className="bg-gray-600 col-span-3">5</div>

      <button onClick={closeSesion}>LOGOUT</button>
    </>
  );
}

export default DashBoard;
