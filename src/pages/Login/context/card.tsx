import axios from "axios";
import { useNavigate } from "react-router-dom";

import React, { useContext, useState } from "react";
import { Link } from "react-router-dom";
// import { createSession } from "../../../helpers/saveLocalStorage";
import { AuthContext } from "../../../context/AuthContext";
interface FormData {
  username: string;
  password: string;
  client_id: string;
  client_secret: string;
  grant_type: string;
}

interface FormErrors {
  username: string;
  password: string;
}

function Card() {
  const { sesion, handleLogin, handleLogout, handleRefreshToken }: any =
    useContext(AuthContext);

  const [formData, setFormData] = useState<FormData>({
    username: "",
    password: "",
    client_id: "python_cl",
    client_secret: "GYP8OCqNimOq0haNj7tcLZ6y7wyWIV1E",
    grant_type: "password",
  });

  const navigate = useNavigate();

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    let isValid = true;
    const errors: FormErrors = {
      username: "",
      password: "",
    };
    if (!formData.username) {
      isValid = false;
      errors.username = "Email is required";
    } else if (!/^\S+@\S+\.\S+$/.test(formData.username)) {
      isValid = false;
      errors.username = "Email is invalid";
    }
    if (!formData.password) {
      isValid = false;
      errors.password = "Password is required";
    }
    // setFormErrors(errors);

    if (isValid) {
      var formBody: any = [];
      console.log(formData);

      formBody = formBody.join("&");
      axios
        .request({
          method: "POST",
          url: "http://localhost:8080/realms/intership/protocol/openid-connect/token",
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            // Authorization: "Basic " + btoa("client-id:applicant_cli"),
          },
          data: formData,
        })
        .then((response) => {
          // createSession(response.data);
          handleLogin(response, formData);
          // console.log(response, "response");
          navigate("/");
        })
        .then((data) => {
          console.log(data);
        })
        .catch(function (errors) {
          console.log(errors);
        });
    }
  };

  function handleChange(event: any) {
    const { name, value, type, checked } = event.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: type === "checkbox" ? checked : value,
    }));
  }

  return (
    <ul className="bg-white rounded-lg w-3/4 text-center px-12 py-6 space-y-4 max-md:space-x-8 max-md:w-screen max-md:h-screen max-md:px-10 max-md:py-24 max-md:rounded-none">
      <img
        src="./src/assets/img/JalaLogo.png"
        alt="Logo Jala Univercity"
        className="w-1/2 mx-auto"
      />
      <p className="font-bold">Login</p>
      <p>Welcome to Jala University</p>
      <form onSubmit={handleSubmit} className="space-y-4 max-md:space-y-6">
        <input
          className="bg-slate-50 w-full p-2 text-base rounded max-md:bg-slate-100"
          type="email"
          name="username"
          placeholder="Email"
          value={formData.username}
          onChange={handleChange}
        />
        <input
          className="bg-slate-50 w-full p-2 text-base rounded max-md:bg-slate-100"
          type="password"
          name="password"
          placeholder="Password"
          value={formData.password}
          onChange={handleChange}
        />
        <div className="block text-left">
          <div className="mb-2 flex content-start max-md:mb-6">
            <input type="checkbox" name="rememberMe" />
            <label className="text-xs px-1">Remenber me</label>
          </div>
          <div className="mb-5 max-md:mb-12">
            <Link className="text-xs font-bold" to={"#"}>
              Reset your password
            </Link>
          </div>
        </div>
        <button
          className="text-white text-base bg-blue-600 w-full rounded p-1 mb-4 max-md:mb-8"
          type="submit"
        >
          Login
        </button>
        <button onClick={() => handleLogin()}>prueba</button>
        <br />
        <hr />
        <button onClick={() => handleLogout()}>logout</button>
      </form>
      <li>
        <p className="text-xs font-semibold max-md:absolute inset-x-0 bottom-0 h-16 pl-6">
          By Jalasoft
        </p>
      </li>
    </ul>
  );
}

export default Card;
