import React from "react";
import Card from "./context/card";

function Login() {
  return (
    <ul className="flex justify-center items-center h-screen w-screen">
      <li className="pl-3.5 max-md:pl-0 max-md:block">
        <Card />
      </li>
      <li className=" pb-28">
        <div className="text-white text-4xl text-opacity-60 space-y-8 pr-3.5 max-md:hidden max-md:pr-0">
          <p>Automation Testing</p>
          <p>FullStack Development</p>
          <p>DevOps</p>
          <p>Quality Assurance</p>
          <p>Movile Development</p>
        </div>
      </li>
    </ul>
  );
}

export default Login;
