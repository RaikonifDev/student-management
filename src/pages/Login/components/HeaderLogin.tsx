import React from "react";
import Logo from "./Logo";

function HeaderLogin() {
  return (
    <>
      <Logo />
      <h2 className="font-semibold text-xl text-center p-4">Login</h2>
      <h3 className="text-center text-gray-600 font-medium">
        Welcome to Jala University
      </h3>
    </>
  );
}

export default HeaderLogin;
