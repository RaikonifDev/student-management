import React from "react";

interface IProps {
  type: string;
  placeholder: string;
}

function InputLogin({ type, placeholder }: IProps) {
  return (
    <input
      className="bg-gray-100 block px-5 py-3 rounded-md"
      name={type}
      type={type}
      maxLength={50}
      placeholder={placeholder}
    />
  );
}

export default InputLogin;
