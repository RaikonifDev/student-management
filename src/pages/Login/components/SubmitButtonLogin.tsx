import React from "react";

interface IProps {
  children: string;
}

function SubmitButtonLogin({ children }: IProps) {
  return (
    <input
      className="block my-5 bg-blueOne p-3 text-xl font-light tracking-wide text-white rounded-md cursor-pointer hover:bg-blue-900 transition-colors"
      type="submit"
      value={children}
    />
  );
}

export default SubmitButtonLogin;
