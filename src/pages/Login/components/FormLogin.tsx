import React from "react";
import InputCheckLogin from "./InputCheckLogin";
import InputLogin from "./InputLogin";
import SubmitButtonLogin from "./SubmitButtonLogin";

function FormLogin() {
  return (
    <form className="flex flex-col gap-5 m-1">
      <InputLogin type={"email"} placeholder={"Email"} />
      <InputLogin type={"password"} placeholder={"Password"} />
      <InputCheckLogin labelText={"Remember me"} />
      <SubmitButtonLogin>Login</SubmitButtonLogin>
    </form>
  );
}

export default FormLogin;
