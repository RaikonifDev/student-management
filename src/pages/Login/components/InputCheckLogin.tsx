import React from "react";

interface IProps {
  labelText: string;
}

function InputCheckLogin({ labelText }: IProps) {
  return (
    <p className="text-gray-700 flex items-center gap-1">
      <input className="" id="remember" type="checkbox" name="remember" />{" "}
      <label htmlFor="remember">{labelText}</label>
    </p>
  );
}

export default InputCheckLogin;
