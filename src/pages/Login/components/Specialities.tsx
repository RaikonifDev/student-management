import React from "react";

function Specialities() {
  return (
    <div className="invisible text-xs grid grid-cols-3 text-white text-opacity-60 tracking-wide font-medium md:visible md:text-5xl md:h-3/5 md:flex md:flex-col md:justify-around">
      <p>Automation Testing</p>
      <p>FullStack Development</p>
      <p>DevOps</p>
      <p>Quality Assurance</p>
      <p>Movile Development</p>
    </div>
  );
}

export default Specialities;
