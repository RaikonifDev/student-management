import React from "react";

function FooterLogin() {
  return (
    <p className="text-gray-600 font-semibold text-base text-center mt-5">
      By Jalasoft
    </p>
  );
}

export default FooterLogin;
