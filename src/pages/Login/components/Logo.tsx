import React from "react";

function Logo() {
  return (
    <img
      src="./src/assets/img/JalaLogo.png"
      alt="Logo Jala Univercity"
      className="w-1/2 mx-auto"
    />
  );
}

export default Logo;
