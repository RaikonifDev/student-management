import React, { useState } from "react";
import { useQuery } from "@tanstack/react-query";

import DataGridTable from "../../components/DataGridTable/Table";

import { getPrograms } from "../../service/programs.service";
import { IProgram } from "../../types/program";
import { useProgramContext } from "./context";

import Add from "../../components/Buttons/Add";
import ModalAddProgram from "./components/ModalAddProgram";
import Header from "./components/Header";

function Programs() {
  const [state, actions] = useProgramContext();
  const [modalAddProgram, setModalAddProgram] = useState(false);
  const { data, refetch } = useQuery({
    queryKey: ["getPrograms"],
    queryFn: getPrograms,
    staleTime: 60000,
  });

  // const headers = [
  //   { name: "#", field: "id" },
  //   { name: "Program Name", field: "program_name" },
  //   { name: "Students", field: "students" },
  //   { name: "Time Spent", field: "time_spent" },
  //   { name: "Status", field: "status" },
  // ];

  // const data = [
  //   { id: 1, name: 'John', email: 'john@example.com', registrationDate: new Date('2022-01-01').toString()},
  //   { id: 2, name: 'Jane', email: 'jane@example.com', registrationDate: new Date('2021-05-01').toString()},
  //   { id: 3, name: 'Bob', email: 'bob@example.com', registrationDate: new Date('2020-12-01').toString()},
  // ];
  const headers = ["id", "program_name", "students", "time_spent", "status"];
  return (
    <>
      <div className="p-4">
        <Header action={setModalAddProgram} setTo={true}></Header>
        <div className="tracking-wide flex justify-between">
          <div className="flex gap-5 items-center">
            <a className="cursor-pointer font-semibold border-b-4 border-blueOne">
              All Programs
            </a>
            <a className="cursor-pointer">In Progress</a>
            <a className="cursor-pointer">Completed</a>
            <a className="cursor-pointer">Pending</a>
          </div>
          <div className="flex items-center gap-4">
            <div className="flex bg-gray-100 rounded-lg overflow-hidden items-center">
              <img
                src="https://img.iFcons8.com/fluency-systems-regular/48/null/calendar--v1.png"
                className="w-9"
              />
              <input
                type="date"
                name="start"
                id="start"
                className="bg-gray-100 px-2"
              />
            </div>
            <span className="font-semibold">To</span>
            <div className="flex bg-gray-100 rounded-lg overflow-hidden items-center">
              <img
                src="https://img.icons8.com/fluency-systems-regular/48/null/calendar--v1.png"
                className="w-9"
              />
              <input
                type="date"
                name="start"
                id="start"
                className="bg-gray-100 px-2"
              />
            </div>
          </div>
        </div>
        <div className="flex flex-col">
          <div className="overflow-x-auto sm:-mx-6 lg:-mx-23">
            <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
              <div className="overflow-hidden">
                <DataGridTable headers={headers} data={state.programs} />
              </div>
            </div>
          </div>
        </div>
        <div className="flex justify-between items-center">
          <p>Showing 1-7 from 132 data</p>
          <div className="flex gap-1">
            <button className="font-black text-xl px-4  rounded-full border-2 border-gray-500">
              &lt;
            </button>
            <div className="flex border-2 border-gray-500 rounded-full gap-3 overflow-hidden">
              <button className="px-4 text-sm rounded-full text-white bg-blueOne">
                1
              </button>
              <button className="p-2 px-4 ">2</button>
              <button className="p-2 px-4 ">3</button>
            </div>
            <button className="font-black text-xl px-4 rounded-full border-2 border-gray-500">
              &gt;
            </button>
          </div>
        </div>
      </div>
      {modalAddProgram ? (
        <ModalAddProgram action={setModalAddProgram} setTo={false} />
      ) : (
        ""
      )}
    </>
  );
}

export default Programs;
