import {
  createContext,
  useContext,
  useReducer,
  useState,
  ReactNode,
} from "react";

import { State, Reducer } from "./store";
import typePrograms from "./dispatchs";
import { IProgram } from "../../../types/program";
import { IProgramState } from "../../../types/context";
import ProgramsActionFactory from "./actions";

const HomeContext = createContext<null | IProgramState>(null);
const HomeDistpach = createContext<any>(null);

interface IProps {
  children: ReactNode;
}

export function ProgramContextProvider(props: IProps) {
  const { children } = props;
  const [value, dispatch] = useReducer(Reducer, props, State);
  const [actions] = useState(ProgramsActionFactory(dispatch));
  return (
    <HomeContext.Provider value={value}>
      <HomeDistpach.Provider value={actions}>{children}</HomeDistpach.Provider>
    </HomeContext.Provider>
  );
}

export function useProgramContext() {
  const state = useContext(HomeContext);
  const actions = useContext(HomeDistpach);
  return [state, actions];
}
