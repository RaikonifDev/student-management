const namespace = "Programs";

const ProgramsDispachsTypes = {
  addProgram: `${namespace}.addProgram`,
};

export default ProgramsDispachsTypes;
