import Programs from "..";
import typePrograms from "./dispatchs";
import { IProgram } from "../../../types/program";

function onAddPrograms(dispatch: any, payload: IProgram[]) {
  dispatch({ type: typePrograms.addProgram, payload: payload });
}

const ProgramsActionFactory = (dispatch: any) => {
  return {
    onAddPrograms: (payload: IProgram[]) => onAddPrograms(dispatch, payload),
  };
};

export default ProgramsActionFactory;
