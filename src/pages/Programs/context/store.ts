import typesPrograms from "./dispatchs";
import { IProgram } from "../../../types/program";
import { IProgramState, IAction } from "../../../types/context";

function State(): IProgramState {
  return {
    programs: new Array<IProgram>(),
  };
}

function Reducer(state: IProgramState, action: IAction) {
  const { type, payload } = action;
  switch (type) {
    case typesPrograms.addProgram: {
      return {
        ...state,
        programs: payload,
      };
    }

    default:
  }
}

export { Reducer, State };
