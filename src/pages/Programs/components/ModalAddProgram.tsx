import React from "react";
import Submit from "../../../components/Buttons/Submit";
import DarkBackground from "../../../components/DarkBackground";
import Form from "../../../components/Form";
import Field from "../../../components/Form/Field";

interface IProps {
  action: Function;
  setTo: any;
}

const ModalAddProgram = ({ action, setTo }: IProps) => {
  return (
    <DarkBackground>
      <Form
        title="Add Program"
        description="Complete the Form"
        action={action}
        setTo={setTo}
      >
        <Field
          labelText="Subject"
          name="subject"
          type="text"
          placeholder="DevOps"
        />
        <Field
          labelText="Duration"
          name="duration"
          type="number"
          placeholder="10"
        />
        <Field labelText="Date Start" name="start" type="date" placeholder="" />
        <Submit />
      </Form>
    </DarkBackground>
  );
};

export default ModalAddProgram;
