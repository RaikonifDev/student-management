import { INavbarItem } from "../types/NavbarItem";

import dashIconBlack from '../assets/icons/dashboardIconBlack.svg'
import dashIconWhite from '../assets/icons/dashboardIconWhite.svg'

import capIconBlack from '../assets/icons/capBlack.svg'
import capIconWhite from '../assets/icons/capWhite.svg'

import scholarIconBlack from '../assets/icons/scholarIconBlack.svg'
import scholarIconWhite from '../assets/icons/scholarIconWhite.svg'

import internshipIconBlack from '../assets/icons/internshipIconBlack.svg'
import internshipIconWhite from '../assets/icons/internshipIconWhite.svg'

import managementIconBlack from '../assets/icons/managementIconBlack.svg'
import managementIconWhite from '../assets/icons/managementIconWhite.svg'

const MENU_ITEMS: INavbarItem[] = [
    {
        name: "Dashboard",
        url: "/",
        blackIconUrl: dashIconBlack,
        whiteIconurl: dashIconWhite,
    },
    {
        name: "Programs",
        url: "/programs",
        blackIconUrl: capIconBlack,
        whiteIconurl: capIconWhite,
    },
    {
        name: "Scholars",
        url: "/scholars",
        blackIconUrl: scholarIconBlack,
        whiteIconurl: scholarIconWhite,
    },
    {
        name: "Internship",
        url: "/internship",
        blackIconUrl: internshipIconBlack,
        whiteIconurl: internshipIconWhite,
    },
    {
        name: "Management",
        url: "/usermanagement",
        blackIconUrl: managementIconBlack,
        whiteIconurl: managementIconWhite,
    },
]

export {
    MENU_ITEMS
}