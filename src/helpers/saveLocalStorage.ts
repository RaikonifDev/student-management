import React from "react";

function saveSession(sessionData: any, credentials: any) {
  const sesionObj = {
    user: sessionData,
    estado: true,
    credentials,
  };
  localStorage.setItem("session", JSON.stringify(sesionObj));
}

const getSesion = () => {
  if (localStorage.getItem("session") !== null) {
    let sessionString: any = localStorage.getItem("session");
    let sessionParse = JSON.parse(sessionString);

    return { ...sessionParse };
  } else {
    return { user: "", estado: false };
  }
};

const logoutSession = () => {
  if (localStorage.getItem("session") !== null) {
    localStorage.removeItem("session");
  }
  console.log("matar sesion");
};

export { saveSession, getSesion, logoutSession };
