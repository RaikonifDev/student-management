import React, { ReactNode } from "react";

interface IProps {
  children: ReactNode;
}

function DarkBackground({ children }: IProps) {
  return (
    <div className="bg-black absolute bottom-0 left-0 right-0 top-0 bg-opacity-60  flex items-center justify-end">
      {children}
    </div>
  );
}

export default DarkBackground;
