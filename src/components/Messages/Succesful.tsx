import React from "react";

interface IProps {
  children: string;
}

function Succesful({ children }: IProps) {
  return (
    <p className=" mx-1 flex justify-center items-center gap-3 bg-greenTwo p-3 text-base md:text-lg font-light tracking-wide text-white rounded-md">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        className="icon icon-tabler icon-tabler-check bg-white rounded-3xl p-0.5"
        width="20"
        height="20"
        viewBox="0 0 24 24"
        strokeWidth="3"
        stroke="#00b341"
        fill="none"
        strokeLinecap="round"
        strokeLinejoin="round"
      >
        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
        <path d="M5 12l5 5l10 -10" />
      </svg>
      {children}
    </p>
  );
}

export default Succesful;
