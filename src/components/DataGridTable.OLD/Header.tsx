interface IProps {
  headers: any[];
}

function Header({ headers }: IProps) {
  return (
    <thead className="border-b">
      <tr>
        {headers.map((header: any) => {
          return (
            <th
              scope="col"
              className="text-sm font-medium text-gray-900 px-6 py-4 text-left"
              key={header.name}
            >
              {header.name}
            </th>
          );
        })}
      </tr>
    </thead>
  );
}

export default Header;
