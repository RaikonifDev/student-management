import React, { useMemo, useState } from "react";

import Header from "./Header";
import Search from "./Search";
import Paginator from "./Paginator";
import { ITEMS_PER_PAGE } from '../../constants/datagridtable.constants';

interface IProps {
  headers: any[];
  rows: any[];
}

function DataGridTable({ headers, rows }: IProps) {
  const [search, setSearch] = useState<String>("");
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [totalItems, setTotalItems] = useState<number>(0);
  const data = useMemo(() => {
    let compute = rows;
    // Here must be other filters
    if (search) {
      compute = compute.filter((row: any) =>
        row.program_name.toLowerCase().includes(search.toLowerCase()),
      );
    }

    setTotalItems(compute.length);
    // after set total items sorting must be here
    compute = compute.slice((currentPage - 1) * ITEMS_PER_PAGE, (currentPage - 1) * ITEMS_PER_PAGE + ITEMS_PER_PAGE);
    console.log(compute);
    return compute;
  }, [search, currentPage, rows]);
  return (
    <>
      <Search onSearch={setSearch} />
      <table className="min-w-full">
        <Header headers={headers} />
        <tbody className="group">
          {data != undefined
            ? data.map((program: any) => {
                const classify_status = function (params: Number) {
                  if (params == 1) {
                    return <div className="text-redTwo"> &#8226; Pending</div>;
                  } else if (params == 2) {
                    return (
                      <div className="text-blueThree"> &#8226; In Progress</div>
                    );
                  } else {
                    return (
                      <div className="text-greenTwo"> &#8226; Completed</div>
                    );
                  }
                };

                return (
                  <tr className="border-b hover:text-white hover:bg-blueOne border-8 border-white border-x-0 " key={program.id}>
                    <td className="px-6 py-4 whitespace-nowrap text-sm font-medium">
                      {program.id}
                    </td>
                    <td className="text-sm  font-light px-6 py-4 whitespace-nowrap">
                      {program.program_name}
                    </td>
                    <td className="text-sm font-light px-6 py-4 whitespace-nowrap">
                      {program.students}
                    </td>
                    <td className="text-sm  font-light px-6 py-4 whitespace-nowrap">
                      {program.time_spent}
                    </td>
                    <td className="text-sm  font-light px-6 py-4 whitespace-nowrap">
                      {classify_status(program.status)}
                    </td>
                  </tr>
                );
              })
            : null}
        </tbody>
      </table>
      <Paginator total={totalItems} itemPerPage={ITEMS_PER_PAGE} currentPage={currentPage} onPageChange={(page: number) => setCurrentPage(page)} />
    </>
  );
}

export default DataGridTable;
