interface IProps {
  onSearch: any;
}

function Search({ onSearch }: IProps) {
  const handleChange = (e: any) => {
    onSearch(e.target.value);
  };
  return (
    <div>
      <label htmlFor="Search">Search</label>
      <input
        className="bg-gray-100 block px-5 py-3 rounded-md"
        name="search"
        type="text"
        maxLength={50}
        placeholder="Search"
        onChange={handleChange}
      />
    </div>
  );
}

export default Search;
