import { useState, useEffect } from "react";
import { NavLink, useLocation } from "react-router-dom";

import { INavbarItem } from "../../types/NavbarItem";

interface IProps extends INavbarItem {
  stateNavbar: boolean;
  isExpanded: boolean;
  changeStateNavbar: (state: boolean) => void;
}

function CustomNavLink({
  name,
  url,
  blackIconUrl,
  whiteIconurl,
  stateNavbar,
  isExpanded,
  changeStateNavbar,
}: IProps) {
  const [urlActive, setUrlActive] = useState(false);
  const actualPath = useLocation();

  useEffect(() => {
    actualPath.pathname == url ? setUrlActive(true) : setUrlActive(false);
  }, [stateNavbar]);

  return (
    <div
      className={
        "relative flex flex-col items-center md:items-start " +
        (urlActive
          ? "before:bg-white before:absolute before:w-[88px] before:h-6 before:top-0 before:inset-y-auto md:before:w-6 md:before:h-24 md:before:top-[-14px] md:before:right-0"
          : "")
      }
    >
      <NavLink
        onClick={() => changeStateNavbar(!stateNavbar)}
        className={({ isActive }) =>
          "my-2 md:my-2 md:py-[26px] md:h-12 w-auto md:w-full flex flex-col md:flex-row items-center md:pr-4 relative  " +
          (isActive
            ? " bg-white rounded-b-full md:rounded-none md:rounded-l-full text-black before:bg-blueOne before:w-12 md:before:w-6 before:h-6 before:absolute before:top-[-8px] before:right-full before:rounded-t-full md:before:right-0 md:before:top-auto md:before:bottom-full md:before:rounded-none md:before:rounded-br-full after:bg-blueOne after:w-12 md:after:w-6 after:h-6 after:absolute after:top-[-8px] after:left-full after:rounded-t-full md:after:left-auto md:after:right-0 md:after:top-full md:after:rounded-none md:after:rounded-tr-full"
            : " z-10 rounded-t-full  md:rounded-r-full text-white ")
        }
        to={url}
      >
        <div className="m-2  z-10 ">
          <img
            className="  w-7 h-7"
            src={urlActive ? blackIconUrl : whiteIconurl}
            alt={name + "Icon"}
          />
        </div>
        <div
          className={
            "hidden md:block z-10 " + (isExpanded ? "md:pl-4" : "md:hidden")
          }
        >
          {name}
        </div>
      </NavLink>
      <div className="pb-2 text-xs sm:text-base md:hidden">{name}</div>
    </div>
  );
}

export default CustomNavLink;
