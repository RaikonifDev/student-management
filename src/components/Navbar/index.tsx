import { useState } from "react";

import CustomNavLink from "./CustomNavLink";

import { MENU_ITEMS } from "../../constants/navbar.constants";
import { INavbarItem } from "../../types/NavbarItem";

interface IProps {
  isExpanded: boolean;
}

function Navbar({ isExpanded }: IProps) {
  const [menuSelection, setMenuSelection] = useState(true);

  return (
    <div className="md:ml-4 md:mt-6 ">
      <nav className="flex md:flex-col justify-around md:justify-start text-white font-rale bg-blueOne rounded-t-2xl rounded-b-3xl md:rounded-r-3xl  ">
        {MENU_ITEMS.map((element: INavbarItem, index: number) => {
          return (
            <CustomNavLink
              key={index}
              name={element.name}
              url={element.url}
              blackIconUrl={element.blackIconUrl}
              whiteIconurl={element.whiteIconurl}
              isExpanded={isExpanded}
              stateNavbar={menuSelection}
              changeStateNavbar={setMenuSelection}
            ></CustomNavLink>
          );
        })}
      </nav>
    </div>
  );
}

export default Navbar;
