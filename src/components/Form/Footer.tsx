import React from "react";

function Footer() {
  return (
    <p className="text-gray-600 font-semibold text-base text-center mt-4 md:mt-10">
      By Jalasoft
    </p>
  );
}

export default Footer;
