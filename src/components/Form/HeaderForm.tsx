import React from "react";
import Logo from "./Logo";

interface IProps {
  title: string;
  description: string;
}

function HeaderForm({ title, description }: IProps) {
  return (
    <div className="flex flex-col gap-3 md:gap-5">
      <Logo />
      <h2 className="font-semibold md:text-xl text-center">{title}</h2>
      <h3 className="text-center text-gray-600 font-medium">{description}</h3>
    </div>
  );
}

export default HeaderForm;
