import React, { FormEvent, ReactElement } from "react";

import Succesful from "../Messages/Succesful";
import Footer from "./Footer";
import HeaderForm from "./HeaderForm";

interface IProps {
  title: string;
  description: string;
  action: Function;
  setTo: any;
  children: any;
}

function Form({ title, description, action, setTo, children }: IProps) {
  function handleClick() {
    action(setTo);
  }
  function handleSubmit(e: FormEvent) {
    e.preventDefault();
    action(setTo);
  }

  return (
    <div className="z-10 m-5 mx-auto md:mr-10 md:fit font-rale text-sm bg-white rounded-3xl max-w-xs shadow-md md:max-w-sm md:text-base">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        className="static icon icon-tabler icon-tabler-x m-4 ml-auto mb-0 cursor-pointer "
        width="24"
        height="24"
        viewBox="0 0 24 24"
        strokeWidth="2"
        stroke="#9e9e9e"
        fill="none"
        strokeLinecap="round"
        strokeLinejoin="round"
        onClick={handleClick}
      >
        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
        <line x1="18" y1="6" x2="6" y2="18" />
        <line x1="6" y1="6" x2="18" y2="18" />
      </svg>
      <div className="flex flex-col justify-between gap-5 md:gap-6 p-6 md:p-7 pt-0 md:px-8 md:pb-6 md:pt-1 md:h-fit md:justify-between">
        <HeaderForm title={title} description={description} />

        <form
          className="flex flex-col m-1"
          action=""
          onSubmit={(e) => handleSubmit(e)}
        >
          {children}
        </form>
        {/* <Succesful>User Added</Succesful> */}
        <Footer />
      </div>
    </div>
  );
}

export default Form;
