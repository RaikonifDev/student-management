import React from "react";

function Logo() {
  return (
    <img
      src="./src/assets/img/JalaLogo.png"
      alt="Logo Jala Univercity"
      className="w-2/3 mx-auto"
    />
  );
}

export default Logo;
