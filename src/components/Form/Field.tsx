import React from "react";

interface IProps {
  labelText: string;
  name: string;
  type: string;
  placeholder: string;
}

function Field({ labelText, name, type, placeholder }: IProps) {
  return (
    <>
      <label className="text-gray-700 md:mb-0.5" htmlFor={name}>
        {labelText}
      </label>
      <input
        className="bg-gray-100 block px-4 py-2.5 rounded-md mb-3 md:mb-4"
        id={name}
        name={name}
        type={type}
        maxLength={50}
        placeholder={placeholder}
      />
    </>
  );
}

export default Field;
