import React from "react";

function Accept() {
  return (
    <button className="block my-4 bg-greenTwo p-2 text-lg md:text-xl font-light tracking-wide text-white rounded-md cursor-pointer hover:bg-greenOne transition-colors md:p-3">
      Accept
    </button>
  );
}

export default Accept;
