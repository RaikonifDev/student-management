import React from "react";
import Basic from "./Basic";

interface IProps {
  action: Function;
  setTo: any;
  children: string;
}

function Add({ action, setTo, children }: IProps) {
  return (
    <Basic
      sign={
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="icon icon-tabler icon-tabler-plus -ml-1"
          width="28"
          height="28"
          viewBox="0 0 24 24"
          strokeWidth="2.5"
          stroke="#ffffff"
          fill="none"
          strokeLinecap="round"
          strokeLinejoin="round"
        >
          <path stroke="none" d="M0 0h24v24H0z" fill="none" />
          <line x1="12" y1="5" x2="12" y2="19" />
          <line x1="5" y1="12" x2="19" y2="12" />
        </svg>
      }
      action={action}
      setTo={setTo}
    >
      {children}
    </Basic>
  );
}

export default Add;
