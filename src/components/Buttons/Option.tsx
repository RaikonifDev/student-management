import React from "react";

interface IProps {
  isActive: Boolean;
  children: string;
}

function Option({ isActive, children }: IProps) {
  return (
    <button
      className={` ${
        isActive
          ? "bg-gray-400 hover:bg-gray-500 cursor-pointer"
          : "bg-gray-300 cursor-default"
      } block my-4 p-2 text-lg md:text-xl font-light tracking-wide text-white rounded-md  transition-colors md:p-3`}
    >
      {isActive ? children : "Disabled"}
    </button>
  );
}

export default Option;
