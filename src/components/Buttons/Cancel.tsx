import React from "react";

function Cancel() {
  return (
    <button className="block my-4 bg-redTwo p-2 text-lg md:text-xl font-light tracking-wide text-white rounded-md cursor-pointer hover:bg-redOne transition-colors md:p-3">
      Cancel
    </button>
  );
}

export default Cancel;
