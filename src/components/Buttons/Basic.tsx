import React, { ReactElement } from "react";

interface IProps {
  sign: ReactElement;
  action: Function;
  setTo: any;
  children: string;
}

function Basic({ sign, action, setTo, children }: IProps) {
  function handleClick() {
    action(setTo);
  }

  return (
    <button
      className="bg-blueTwo hover:bg-blueOne text-white font-rale px-5 flex gap-4 justify-center items-center rounded-xl shadow-sm"
      onClick={handleClick}
    >
      <div className="text-3xl font-extralight flex items-center gap-2">
        {sign}
        <span className="text-4xl font-extralight inline">I</span>
      </div>

      <div className="font-light tracking-wider ">{children}</div>
    </button>
  );
}

export default Basic;
