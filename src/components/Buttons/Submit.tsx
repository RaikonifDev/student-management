import React from "react";

function Submit() {
  return (
    <button className="block my-4 bg-blueTwo p-3 text-lg md:text-xl font-light tracking-wide text-white rounded-md cursor-pointer hover:bg-blueOne transition-colors md:p-3">
      Submit
    </button>
  );
}

export default Submit;
