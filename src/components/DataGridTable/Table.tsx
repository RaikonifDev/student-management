import React, { useState, useEffect } from "react";

import SearchBar from "./SearchBar";
import SortButton from "./SortButton";
import Pagination from "./Pagination";

interface TableProps {
  data: object[];
  headers: string[];
}

interface TableState {
  data: object[];
  headers: string[];
  sortBy: any;
  sortOrder: "asc" | "desc";
  currentPage: number;
  pageSize: number;
  searchTerm: string;
}

const Table: React.FC<TableProps> = ({ data, headers }) => {
  useEffect(() => {
    setTableState((prevState) => {
      return {
        ...prevState,
        data: data,
      };
    });
    console.log(data);
  }, [data]);

  const [tableState, setTableState] = useState<TableState>({
    data,
    headers,
    sortBy: "",
    sortOrder: "asc",
    currentPage: 1,
    pageSize: 10,
    searchTerm: "",
  });
  const sortData = (data: object[], sortBy: any, sortOrder: "asc" | "desc") => {
    return data.sort((a: any, b: any) => {
      const valueA = a[sortBy];
      const valueB = b[sortBy];
      if (sortOrder === "asc") {
        if (valueA < valueB) {
          return -1;
        }
        if (valueA > valueB) {
          return 1;
        }
        return 0;
      } else {
        if (valueA > valueB) {
          return -1;
        }
        if (valueA < valueB) {
          return 1;
        }
        return 0;
      }
    });
  };

  const filterData = (data: object[], searchTerm: string) => {
    if (!searchTerm) {
      return data;
    }
    return data.filter((item) =>
      Object.values(item).some(
        (val) =>
          typeof val === "string" &&
          val.toLowerCase().includes(searchTerm.toLowerCase()),
      ),
    );
  };

  const getPaginatedData = (tableState: TableState) => {
    const { data, currentPage, pageSize } = tableState;
    const startIndex = (currentPage - 1) * pageSize;
    const endIndex = startIndex + pageSize;
    return data.slice(startIndex, endIndex);
  };

  const handleSort = (header: string) => {
    setTableState((prevState) => {
      const newSortOrder =
        prevState.sortBy === header && prevState.sortOrder === "asc"
          ? "desc"
          : "asc";
      return {
        ...prevState,
        sortBy: header,
        sortOrder: newSortOrder,
        data: sortData(prevState.data, header, newSortOrder),
      };
    });
  };

  const handlePageChange = (newPage: number) => {
    setTableState((prevState) => ({
      ...prevState,
      currentPage: newPage,
    }));
  };

  const handleSearch = (searchTerm: string) => {
    setTableState((prevState) => ({
      ...prevState,
      searchTerm,
      data: filterData(prevState.data, searchTerm),
    }));
  };

  const paginatedData = getPaginatedData(tableState);

  return (
    <div>
      {/* <SearchBar headers={headers} onSearch={handleSearch} /> */}
      <div>
        {/* <button onClick={()=> {handleSearch("Dev Program 1")}}>
          In Progress
        </button>
        <button onClick={()=> {handleSearch("Pending")}}>
          Pending
        </button>
        <button onClick={()=> {handleSearch("Completed")}}>
          Completed
        </button> */}
      </div>
      <table>
        <thead>
          <tr>
            {headers.map((header) => (
              <th key={header}>
                {header}{" "}
                <SortButton
                  onClick={() => handleSort(header)}
                  active={tableState.sortBy === header}
                  sortOrder={tableState.sortOrder}
                />
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {paginatedData.map((row: any) => (
            <tr key={row.id}>
              {headers.map((header) => (
                <td key={`${row.id}-${header}`}>
                  {
                    // switch (typeof row[header]) {
                    //   case "boolean":
                    //     break;
                    //     // return CHECKBOX;
                    //     // Modify STATE
                    //   default:
                    //     break;
                    // }
                    row[header]
                  }
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
      <Pagination
        currentPage={tableState.currentPage}
        pageSize={tableState.pageSize}
        total={tableState.data.length}
        onPageChange={handlePageChange}
      />
    </div>
  );
};

export default Table;

// return (
//   <div>
//     <ColumnSelector
//       headers={headers}
//       onChange={(visibleHeaders) => {
//         setVisibleHeaders(visibleHeaders);
//         if (onVisibleHeadersChange) {
//           onVisibleHeadersChange(visibleHeaders);
//         }
//       }}
//     />
//     <table>
//       <thead>
//         <tr>
//           {visibleHeaders.map((header) => (
//             <th key={header}>{header}</th>
//           ))}
//         </tr>
//       </thead>
//       <tbody>
//         {filteredData.map((row) => (
//           <tr key={row.id}>
//             {visibleHeaders.map((header) => (
//               <td key={header}>{row[header]}</td>
//             ))}
//           </tr>
//         ))}
//       </tbody>
//     </table>
//   </div>
// );

// return (
//   <div>
//     <SearchBar headers={headers} onSearch={handleSearch} />
//     <table>
//       <thead>
//         <tr>
//           {headers.map((header) => (
//             <th key={header}>
//               {header}{' '}
//               <SortButton
//                 onClick={() => handleSort(header)}
//                 active={tableState.sortBy === header}
//                 sortOrder={tableState.sortOrder}
//               />
//             </th>
//           ))}
//         </tr>
//       </thead>
//       <tbody>
//         {paginatedData.map((row: any) => (
//           <tr key={row.id}>
//             {headers.map((header) => (
//               <td key={`${row.id}-${header}`}>{row[header]}</td>
//             ))}
//           </tr>
//         ))}
//       </tbody>
//     </table>
//     <Pagination
//       currentPage={tableState.currentPage}
//       pageSize={tableState.pageSize}
//       total={tableState.data.length}
//       onPageChange={handlePageChange}
//     />
//   </div>
// );
