import React, { useState } from 'react';

interface SearchBarProps {
  headers: string[];
  onSearch: (searchTerm: string, headers: string[]) => void;
}

const SearchBar: React.FC<SearchBarProps> = ({ headers, onSearch }) => {
  const [searchTerm, setSearchTerm] = useState('');

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    onSearch(searchTerm, headers);
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        placeholder="Search..."
        value={searchTerm}
        onChange={(event) => setSearchTerm(event.target.value)}
      />
      <button type="submit">Search</button>
    </form>
  );
};

export default SearchBar;