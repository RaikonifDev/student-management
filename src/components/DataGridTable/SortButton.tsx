import React from 'react';

interface SortButtonProps {
  onClick: () => void;
  active: boolean;
  sortOrder: 'asc' | 'desc';
}

const SortButton: React.FC<SortButtonProps> = ({ onClick, active, sortOrder }) => {
  return (
    <button onClick={onClick}>
      {active ? (sortOrder === 'asc' ? '▲' : '▼') : 'Sort'}
    </button>
  );
};

export default SortButton;
