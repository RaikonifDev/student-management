import { Navigate } from "react-router-dom";
import { useContext } from "react";
import { AuthContext } from "../context/AuthContext";

export const ProtectedRoute = ({ children }: any) => {
  const { sesion, handleLogout } = useContext(AuthContext);
  const { estado } = sesion;
  if (!estado) {
    // user is not authenticated
    return <Navigate to="/login" />;
  }
  return children;
};
