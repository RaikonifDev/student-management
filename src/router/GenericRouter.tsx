import Generic from "../layouts/Generic";
import DashBoard from "../pages/DashBoard";
import Programs from "../pages/Programs";
import Scholars from "../pages/Scholars";
import Internship from "../pages/Internship";
import UserManagement from "../pages/UserManagement";
import NotFound from "../pages/NotFound";
import { ProgramContextProvider } from "../pages/Programs/context";
import { UsersManagementContextProvider } from "../pages/UserManagement/context";

const GenericRoutes = {
  path: "/",
  element: <Generic />,
  children: [
    {
      path: "",
      element: <DashBoard />,
    },
    {
      path: "/programs",
      element: (
        <ProgramContextProvider>
          <Programs />
        </ProgramContextProvider>
      ),
    },
    {
      path: "/scholars",
      element: <Scholars />,
    },
    {
      path: "/internship",
      element: <Internship />,
    },
    {
      path: "/usermanagement",
      element: (
        <UsersManagementContextProvider>
          <UserManagement />
        </UsersManagementContextProvider>
      ),
    },
    {
      path: "/*",
      element: <NotFound />,
    },
  ],
};

export default GenericRoutes;
