import Authentification from "../layouts/Authentification";
import Login from "../pages/Login";

const AuthentificationRouter = {
  path: "/login",
  element: <Authentification />,
  children: [
    {
      path: "",
      element: <Login />,
    },
  ],
};

export default AuthentificationRouter;
