import { createContext, useState } from "react";

import {
  saveSession,
  getSesion,
  logoutSession,
} from "../helpers/saveLocalStorage";

const AuthContext = createContext<null | any>(null);

function AuthProvider({ children }: any) {
  const initialSession = getSesion();

  const [sesion, setSesion] = useState(initialSession);

  const handleLogin = (response: any, credentials: any) => {
    setSesion({ ...sesion, estado: true, credentials });
    saveSession(response, credentials);
  };
  const handleLogout = () => {
    setSesion({ ...sesion, user: "", estado: false });
    logoutSession();
    // destroy session in keycloak
  };

  const handleRefreshToken = () => {};

  const option = { sesion, handleLogin, handleLogout, handleRefreshToken };

  return <AuthContext.Provider value={option}>{children}</AuthContext.Provider>;
}

export { AuthContext, AuthProvider };
