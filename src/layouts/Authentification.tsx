import React from "react";
import { Outlet } from "react-router-dom";

function Authentification() {
  return (
    <div className="bg-[url('./assets/img/JalaCompany.png')] bg-cover bg-center bg-no-repeat grayscale-0">
      <div className="bg-blueOne bg-opacity-60 h-screen w-full">
        <Outlet />
      </div>
    </div>
  );
}

export default Authentification;
