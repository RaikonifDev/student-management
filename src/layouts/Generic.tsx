import { useState } from "react";
import { Outlet } from "react-router-dom";

import Navbar from "../components/Navbar";

import shortLogo from "../assets/icons/short_logo.png";
import expandedLogo from "../assets/icons/expanded_logo.png";

function Generic() {
  const [isExpanded, setIsExpanded] = useState(false);

  function changeOnClick() {
    setIsExpanded(!isExpanded);
  }

  return (
    <div className="flex ">
      <div
        className={
          "hidden md:block " + (isExpanded ? "lg:w-[220px] " : "md:w-[80px] ")
        }
      ></div>

      <div
        className={
          "md:h-full md:rounded-r-3xl md:bg-blue fixed bottom-0 w-full md:top-0 md:left-0 " +
          (isExpanded ? "md:w-[220px]" : "md:w-[80px]")
        }
      >
        <header className="relative">
          <div className="absolute w-full bg-[#464646] h-2/4 bottom-0 md:hidden"></div>
          <div className="flex flex-col mt-0 md:mt-8 w-full relative z-20">
            <button
              className="hidden md:flex w-full justify-center my-6 items-center text-white bg-blue z-10 rounded-r-full "
              onClick={changeOnClick}
            >
              <img
                className="w-auto h-12"
                src={isExpanded ? expandedLogo : shortLogo}
                alt=""
              />
            </button>
            <Navbar isExpanded={isExpanded}></Navbar>
          </div>
        </header>
      </div>
      <div className="ml-0 md:ml-4 grow">
        <Outlet />
      </div>
    </div>
  );
}

export default Generic;
