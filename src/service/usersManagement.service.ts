import service from "./index";

const getUsers = async () => {
  const response = await service.get("/users");
  return response.data;
};

const putUsers = async () => {
  const response = await service.get("/users");
  return response.data;
};

export { getUsers, putUsers };
