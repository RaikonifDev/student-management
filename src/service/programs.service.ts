import service from "./index";

const getPrograms = async () => {
  const response = await service.get("/programs");
  return response.data;
};

const putPrograms = async () => {
  const response = await service.get("/programs");
  return response.data;
};

export { getPrograms, putPrograms };
