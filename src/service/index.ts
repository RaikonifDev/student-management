import axios, {AxiosRequestConfig, AxiosError, AxiosResponse, AxiosHeaders} from 'axios';

import { BASE_URL_API } from '../constants/service.constants';

const service = axios.create({ baseURL: BASE_URL_API, headers: { 'Content-Type': 'application/json' } });
const onRequest = (config: AxiosRequestConfig): AxiosRequestConfig => {

    // const token = "bearer dsdndofndsjfnodinsaid8237e823idasd7a8dauda8";
    // if (token) {
    //     config.headers = { ...config.headers, Authorization: `${token}` } as AxiosHeaders;
    // }
    return config;
}

const onRequestError = (error: AxiosError): Promise<AxiosError> => {
    return Promise.reject(error);
}

const onResponse = (response: AxiosResponse) => {
    return response;
}

const onResponseError = (error: AxiosError): Promise<AxiosError> => {
    return Promise.reject(error);
}

service.interceptors.request.use(onRequest, onRequestError);
service.interceptors.response.use(onResponse, onResponseError);

export default service;